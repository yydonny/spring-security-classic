package yd.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import yd.model.security.Authority;
import yd.model.security.AuthorityRepository;
import yd.model.security.User;
import yd.model.security.UserRepository;

/**
 * Initialize the UserDetailsService database on startup.
 * Only for demo purpose.
 */
@Component
public class UserDetailsInitialization implements CommandLineRunner {
  private final AuthorityRepository authorityRepository;
  private final UserRepository userRepository;
  private final PlatformTransactionManager transactionManager;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public UserDetailsInitialization(AuthorityRepository authorityRepository, UserRepository userRepository,
                                   PlatformTransactionManager transactionManager, PasswordEncoder passwordEncoder)
  {
    this.authorityRepository = authorityRepository;
    this.userRepository = userRepository;
    this.transactionManager = transactionManager;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public void run(String... args) throws Exception {
    new TransactionTemplate(transactionManager).execute(transactionStatus -> {
      userRepository.save(new User("yangyd", passwordEncoder.encode("asdf1234")));
      userRepository.save(new User("user1", passwordEncoder.encode("asdf1234")));

      authorityRepository.save(new Authority("yangyd", "ADMIN"));
      authorityRepository.save(new Authority("user1", "USER"));
      return null;
    });
  }
}
