package yd.data;

import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * Using Spring boot's default embedded JPA configuration.
 */
@EntityScan(basePackages = "yd.model")
public class DatabaseConfig {
}
