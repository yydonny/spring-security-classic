package yd.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletRequestAttributes;
import yd.model.ErrorResponse;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * Render error information gathered by Spring Boot
 *
 * For unauthorized access, an errorAttribute of 403 status is generated without exception.
 * Also the principal is not available here. (SecurityContextHolder.getContext().getAuthentication() returns null)
 */
@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
class MyErrorController implements ErrorController {
  private static final Logger logger = LoggerFactory.getLogger(MyErrorController.class);
  private static final String SERVLET_STATUS_CODE = "javax.servlet.error.status_code";
  private final ErrorAttributes errorAttributes;

  @NotNull
  @Value("${server.error.path:${error.path:/error}}")
  private String errorPath;

  @Autowired
  MyErrorController(ErrorAttributes errorAttributes) {
    this.errorAttributes = errorAttributes;
  }

  @Override
  public String getErrorPath() {
    return errorPath;
  }

  @RequestMapping
  @ResponseBody
  ResponseEntity<ErrorResponse> renderError(HttpServletRequest request) {
    // @see org.springframework.boot.autoconfigure.web.DefaultErrorAttribdefautes
    Map<String, Object> error = errorAttributes.getErrorAttributes(new ServletRequestAttributes(request), false);

    ErrorResponse errorResponse = new ErrorResponse(
        String.valueOf(error.get("error")), String.valueOf(error.get("message")));

    logError(error);
    return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(statusCode(request)));
  }

  private void logError(Map<String, Object> error) {
    Object detail = error.get("exception");
    if (detail == null) {
      detail = error.get("error");
    }
    logger.error("Exception caught in {} - {}: {}", error.get("path"), detail, error.get("message"));
  }

  private int statusCode(HttpServletRequest request) {
    Integer statusCode = (Integer) request.getAttribute(SERVLET_STATUS_CODE);
    if (statusCode == null) {
      statusCode = 500;
    }
    return statusCode;
  }
}
