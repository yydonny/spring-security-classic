package yd.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
class AdminController {
  Logger logger = LoggerFactory.getLogger(AdminController.class);

  /**
   * retrieve current UserDetails from SecurityContext.
   */
  private User currentUser() {
    return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  @RequestMapping
  public String index(@AuthenticationPrincipal User user, Model model) {
    List<String> roleList = user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
    model.addAttribute("username", user.getUsername());
    model.addAttribute("role", String.join(" ", roleList));
    return "admin-index";
  }
}
