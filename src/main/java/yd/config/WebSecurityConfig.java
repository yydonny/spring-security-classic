package yd.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

/**
 * Decoupled AuthenticationManager config and HttpSecurity config.
 * AuthenticationManager config doesn't need to be inside a @component or @configuration bean.
 *
 * It is important to only configure AuthenticationManagerBuilder in a class annotated with
 * either @EnableWebSecurity, @EnableGlobalMethodSecurity, or @EnableGlobalAuthentication.
 * Doing otherwise has unpredictable results.
 *
 * The @EnableWebSecurity annotation completely switch off the auto configuration by spring-boot.
 * (this does not disable the authentication manager configuration or Actuator’s security).
 *
 * @see <a href="http://docs.spring.io/spring-boot/docs/1.4.4.RELEASE/reference/htmlsingle/#boot-features-security">http://docs.spring.io/spring-boot/docs/1.4.4.RELEASE/reference/htmlsingle/#boot-features-security</a>
 */
@EnableWebSecurity
class WebSecurityConfig {
  @Autowired
  private AuthenticationProvider authenticationProvider;

  @Autowired
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authenticationProvider);
  }

  /**
   * Security config based on form login.
   */
  @Component
  static class FormLogin extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http.authorizeRequests() // Access control. Order matters!
          .mvcMatchers("/admin/**").hasAuthority("ADMIN")
          .antMatchers("/db/**").access("hasRole('ADMIN') and hasRole('DBA')") // hasRole('ADMIN') means hasAuthority('ROLE_ADMIN')
          .anyRequest().authenticated() // allow other resources to be accessed by normal users
          .and()

          // enable form-based login http://docs.spring.io/spring-security/site/docs/4.1.4.RELEASE/reference/htmlsingle/#jc-form
          // Unauthorized request is redirected to /login; a POST to /login attempt to authenticate the user
          // CSRF is enabled by default
          .formLogin()
          .loginPage("/login").permitAll()
          .and()

          // Handling logout
          // Post to /logout to logout, then redirect to /
          // CSRF is required as for login
          .logout().logoutUrl("/logout").logoutSuccessUrl("/")
          .invalidateHttpSession(true)
          .deleteCookies("somecookie");
    }
  }
}
