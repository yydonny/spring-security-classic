package yd.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
  private String code;
  private String reason;
  private String details;

  public ErrorResponse(String code) {
    this.code = code;
  }

  public ErrorResponse(String code, String reason) {
    this.code = code;
    this.reason = reason;
  }

  public ErrorResponse(String code, String reason, String details) {
    this.code = code;
    this.reason = reason;
    this.details = details;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }
}
